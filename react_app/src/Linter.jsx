import React from 'react';
import classnames from 'classnames';

import { lint_yaml } from './model';
import './Linter.css';

const mapper = (item, index) => <div className='Item'
                                     key={`Error_Item_${index}`}>{item}</div>;

const Linter = ({ list }) => {
    const has_errors = list.length > 0;
    const classes = classnames({ Errors: has_errors });
    return <div id='Linter'
                className={classes}>
        {list.map(mapper)}
        <button onClick={() => lint_yaml()}>{has_errors ? 'Lint again' : 'Yaml linter'}</button>
    </div>;
};

export default Linter;
