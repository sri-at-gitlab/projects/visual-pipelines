import React from 'react';
import classnames from 'classnames';

class Library extends React.Component {
    constructor(props) {
        super(props);
        this.state = { selected: null };
    }

    render() {
        const { selected } = this.state;
        const { items, on_click_import } = this.props;
        const item_elements = items.map(item => {
            const class_names = classnames({ Item: true, Selected: selected === item.path });
            return <div className={class_names}
                        key={`Pipelib_Library_Item_${item.path}`}
                        onClick={() => this.setState({ selected: item.path })}>
                <img src={item.icon} className='Icon'/>
                <div>
                    <div className='Title'>{item.title}</div>
                    <div className='Author'>{item.author_name} &#60;{item.author_email}&#62;</div>
                    <div className='Description'>{item.description}</div>
                </div>
                <div className='Actions'>
                    <a onClick={() => on_click_import(item)}>Import</a>
                </div>
            </div>;
        });
        return <div id='Library'>
            {item_elements}
        </div>;
    }
}

export default Library;
