import React from 'react';
import model, { pipeline_set_job_variables } from '../../model';

class VariablesField extends React.Component {
    constructor(props) {
        super(props);
        this.state = { variables: model.pipeline[model.job_editor_name].variables || {} };
    }

    render() {
        const { variables } = this.state;
        const keys = Object.keys(variables);
        keys.push('');

        const fields = keys.map(key => {
            const value = variables[key] || null;
            return <div className='SubField'
                        key={`SubField_${key}`}>
                <div className='Label'>
                    <input className='Input'
                           data-key={key}
                           defaultValue={key}
                           onChange={event => this.on_change_key(event)}
                           placeholder='Key'
                           type='text'/>
                </div>
                <input className='Input'
                       data-key={key}
                       defaultValue={value}
                       disabled={!key}
                       onChange={event => this.on_change_value(event)}
                       placeholder='Value'
                       type='text'/>
            </div>;
        });

        return <div className='Field'>
            <div className='Label'>
                Variables
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#variables'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Define variables that are passed to the job environment
                </div>
            </div>
            <div className='SubForm'>{fields}</div>
        </div>;
    }

    on_change_key(event) {
        const { variables } = this.state;
        const target = event.target;
        const original_key = target.getAttribute('data-key');
        const new_key = target.value;
        if (!original_key) {
            variables[new_key] = '';
            this.setState({ variables }, () => {
                target.value = '';
                document.querySelector(`.Input[data-key='${new_key}']`).focus();
                this.on_change();
            });
        }
        else if (new_key) {
            variables[new_key] = variables[original_key];
            delete variables[original_key];
            this.setState({ variables }, () => {
                document.querySelector(`.Input[data-key='${new_key}']`).focus();
                this.on_change();
            });
        }
        else {
            delete variables[original_key];
            this.setState({ variables }, () => this.on_change());
        }
    }


    on_change_value(event) {
        const { variables } = this.state;
        const key = event.target.getAttribute('data-key');
        variables[key] = event.target.value;
        this.setState({ variables }, () => this.on_change());
    }

    on_change() {
        pipeline_set_job_variables(model.job_editor_name, this.state.variables);
    }
}

export default VariablesField;
