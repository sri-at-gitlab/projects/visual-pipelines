import React from 'react';
import model, { get_all_mixins, pipeline_set_job_extends } from '../../model';

class ExtendsField extends React.Component {
    render() {
        const selected_mixins = model.pipeline[model.job_editor_name].extends || [];
        const mixins = get_all_mixins();
        const mapper = mixin => {
            return <option key={`mixin_${mixin}`}>{mixin}</option>;
        };
        const options = mixins.map(mapper);
        return <label className='Field'>
            <div className='Label'>
                Extends
                <a href='https://docs.gitlab.com/ee/ci/yaml/#extends'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to define list of mixins that this job will extend
                </div>
            </div>
            <select className='Input'
                    defaultValue={selected_mixins}
                    multiple
                    onChange={event => this.on_change(event)}
                    size={Math.min(6, mixins.length)}>{options}</select>
        </label>;
    }

    on_change(event) {
        const selected_values = [...event.target.selectedOptions].map(o => o.value);
        pipeline_set_job_extends(model.job_editor_name, selected_values);
    }
}

export default ExtendsField;
