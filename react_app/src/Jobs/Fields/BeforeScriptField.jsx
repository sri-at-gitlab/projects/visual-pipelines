import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_before_script } from '../../model';

class BeforeScriptField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];

        return <div className='Field'>
            <div className='Label'>
                Before Script
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#before_script-and-after_script'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to define a command that should be run before each job, including deploy jobs, but after the
                    restoration of any artifacts.
                </div>
            </div>
            <StringListInput key_prefix='Before_Script'
                             list={job.before_script || []}
                             on_change={value => this.on_change(value)}
                             placeholder_add='Add before script'
                             value_prefix='$ '/>
        </div>;
    }

    on_change(value) {
        pipeline_set_job_before_script(model.job_editor_name, value);
    }
}

export default BeforeScriptField;
