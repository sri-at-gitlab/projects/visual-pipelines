import React from 'react';
import model, {
    pipeline_set_job_trigger,
    pipeline_set_job_trigger_branch,
    pipeline_set_job_trigger_include,
    pipeline_set_job_trigger_project,
    pipeline_set_job_trigger_strategy,
} from '../../model';

class TriggerField extends React.Component {
    constructor(props) {
        super(props);
        this.state = { mode: null };
    }


    render() {
        const { mode } = this.state;
        const trigger = model.pipeline[model.job_editor_name].trigger || {};

        let subfields = null;

        if (mode === 'multi_project') {
            subfields = <>
                <div className='SubField'>
                    <div className='Label'>Project</div>
                    <input className='Input'
                           defaultValue={trigger.project}
                           onChange={event => this.on_change_project(event)}
                           placeholder='Project path'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Branch</div>
                    <input className='Input'
                           defaultValue={trigger.branch}
                           onChange={event => this.on_change_branch(event)}
                           placeholder='Branch'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Strategy</div>
                    <select className='Input'
                            defaultValue={trigger.strategy}
                            onChange={event => this.on_change_strategy(event)}>
                        <option value={null}>None</option>
                        <option value='depend'>Depend</option>
                    </select>
                </div>
            </>;
        }
        else if (mode === 'parent_child') {
            subfields = <>
                <div className='SubField'>
                    <div className='Label'>Include</div>
                    <input className='Input'
                           defaultValue={trigger.include}
                           onChange={event => this.on_change_include(event)}
                           placeholder='Local file path'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Branch</div>
                    <input className='Input'
                           defaultValue={trigger.branch}
                           onChange={event => this.on_change_branch(event)}
                           placeholder='Branch'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Strategy</div>
                    <select className='Input'
                            defaultValue={trigger.strategy}
                            onChange={event => this.on_change_strategy(event)}>
                        <option value={null}>None</option>
                        <option value='depend'>Depend</option>
                    </select>
                </div>
            </>;
        }

        return <div className='Field'>
            <div className='Label'>
                Trigger
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#trigger'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Define downstream pipeline triggers allowing multi project pipelines or parent-child pipelines
                </div>
            </div>
            <div className='SubForm'>
                <div className='SubField'>
                    <div className='Label'>Mode</div>
                    <select className='Input'
                            defaultValue={mode}
                            onChange={event => this.on_change_mode(event)}>
                        <option value={null}>None</option>
                        <option value='multi_project'>Multi-Project Pipeline</option>
                        <option value='parent_child'>Parent-Child Pipeline</option>
                    </select>
                </div>
                {subfields}
            </div>
        </div>;
    }

    on_change_mode(event) {
        const mode = event.target.value;
        pipeline_set_job_trigger(model.job_editor_name, {});
        this.setState({ mode });
    }

    on_change_branch(event) {
        const value = event.target.value;
        pipeline_set_job_trigger_branch(model.job_editor_name, value || null);
    }

    on_change_strategy(event) {
        const value = event.target.value;
        pipeline_set_job_trigger_strategy(model.job_editor_name, value || null);
    }

    on_change_project(event) {
        const value = event.target.value;
        pipeline_set_job_trigger_project(model.job_editor_name, value || null);
        pipeline_set_job_trigger_include(model.job_editor_name, null);
    }

    on_change_include(event) {
        const value = event.target.value;
        pipeline_set_job_trigger_include(model.job_editor_name, value || null);
        pipeline_set_job_trigger_project(model.job_editor_name, null);
    }
}

export default TriggerField;
