import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_script } from '../../model';

class ScriptField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];

        return <div className='Field'>
            <div className='Label'>
                Script
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#script'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    The only mandatory keyword that a job needs. It's one or more shell scripts which is executed by the
                    Runner.
                </div>
            </div>
            <StringListInput key_prefix='Script'
                             list={job.script || []}
                             on_change={value => this.on_change(value)}
                             placeholder_add='Add script'
                             value_prefix='$ '/>
        </div>;
    }

    on_change(value) {
        pipeline_set_job_script(model.job_editor_name, value);
    }
}

export default ScriptField;
