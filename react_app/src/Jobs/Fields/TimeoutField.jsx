import React from 'react';
import model, { pipeline_set_job_timeout } from '../../model';

class TimeoutField extends React.Component {
    render() {
        const timeout = model.pipeline[model.job_editor_name].timeout;

        return <label className='Field'>
            <div className='Label'>
                Timeout
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#timeout'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Allows you to configure a timeout for a specific job
                </div>
            </div>
            <div>
                <input className='Input'
                       defaultValue={timeout}
                       onChange={event => this.on_change(event)}
                       type='text'/>
            </div>
        </label>;
    }

    on_change(event) {
        pipeline_set_job_timeout(model.job_editor_name, event.target.value);
    }
}

export default TimeoutField;
