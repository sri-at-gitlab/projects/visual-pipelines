import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_image, pipeline_set_job_release } from '../../model';

class ReleaseField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            use_image: false,
            release: { tag_name: '', name: '', description: '', ref: '', milestones: [] },
        };
    }

    render() {
        const { use_image, release } = this.state;

        return <div className='Field'>
            <div className='Label'>
                Release
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#release'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Indicates that the job creates a Release and optionally includes URLs for Release assets
                </div>
            </div>
            <div className='SubForm'>
                <div className='SubField'>
                    <div className='Label'>Use Release CLI Image</div>
                    <div>
                        <select className='Input'
                                defaultValue={use_image}
                                onChange={event => this.on_change_use_image(event)}>
                            <option>No</option>
                            <option>Yes</option>
                        </select>
                    </div>
                </div>
                <div className='SubField'>
                    <div className='Label'>Tag name</div>
                    <input className='Input'
                           defaultValue={release.tag_name}
                           disabled={!use_image}
                           onChange={event => this.on_change_tag_name(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Name</div>
                    <input className='Input'
                           defaultValue={release.name}
                           disabled={!use_image}
                           onChange={event => this.on_change_name(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Description</div>
                    <input className='Input'
                           defaultValue={release.description}
                           disabled={!use_image}
                           onChange={event => this.on_change_description(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Ref</div>
                    <input className='Input'
                           defaultValue={release.ref}
                           disabled={!use_image}
                           onChange={event => this.on_change_ref(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Milestones</div>
                    <StringListInput disabled={!use_image}
                                     list={release.milestones}
                                     on_change={list => this.on_change_milestones(list)}/>
                </div>
            </div>
        </div>;
    }

    on_change_use_image(event) {
        const value = event.target.value;
        const is_yes = value === 'Yes';
        if (is_yes) {
            if (window.confirm('This will set image to Release CLI. Continue?')) {
                this.setState({ use_image: true });
                pipeline_set_job_image(model.job_editor_name, 'registry.gitlab.com/gitlab-org/release-cli:latest');
            }
        }
        else {
            this.setState({ use_image: false });
            pipeline_set_job_image(model.job_editor_name, null);
        }
    }

    on_change_tag_name(event) {
        const value = event.target.value;
        const { release } = this.state;
        release.tag_name = value;
        pipeline_set_job_release(model.job_editor_name, release);
    }

    on_change_name(event) {
        const value = event.target.value;
        const { release } = this.state;
        release.name = value;
        pipeline_set_job_release(model.job_editor_name, release);
    }

    on_change_description(event) {
        const value = event.target.value;
        const { release } = this.state;
        release.description = value;
        pipeline_set_job_release(model.job_editor_name, release);
    }

    on_change_ref(event) {
        const value = event.target.value;
        const { release } = this.state;
        release.ref = value;
        pipeline_set_job_release(model.job_editor_name, release);
    }

    on_change_milestones(value) {
        const { release } = this.state;
        release.milestones = value;
        pipeline_set_job_release(model.job_editor_name, release);
    }
}

export default ReleaseField;
