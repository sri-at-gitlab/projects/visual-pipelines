import React from 'react';
import model, { pipeline_set_job_interruptible } from '../../model';

class InterruptibleField extends React.Component {
    render() {
        return <label className='Field'>
            <div className='Label'>
                Interruptible
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#interruptible'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to indicate that a job should be canceled if made redundant by a newer pipeline run
                </div>
            </div>
            <div>
                <select className='Input'
                        onChange={event => this.on_change(event)}>
                    <option>False</option>
                    <option>True</option>
                </select>
            </div>
        </label>;
    }

    on_change(event) {
        pipeline_set_job_interruptible(model.job_editor_name, event.target.value === 'True');
    }
}

export default InterruptibleField;
