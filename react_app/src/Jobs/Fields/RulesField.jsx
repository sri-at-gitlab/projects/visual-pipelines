import React from 'react';
import model, { pipeline_set_job_rules } from '../../model';
import RuleClauseInput from '../../Inputs/RuleClauseInput';
import RuleClause from '../../Inputs/RuleClause';

class RulesField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected_index: null,
            rules: model.pipeline[model.job_editor_name].rules || [],
        };
    }

    render() {
        const { rules, selected_index } = this.state;

        const clauses = rules.map((clause, index) => {
            if (index === selected_index) {
                return <RuleClauseInput clause={clause}
                                        index={index}
                                        key={`rules_field_clause_input_${index}`}
                                        on_delete={index => this.on_click_delete(index)}
                                        on_save={(clause, index) => this.on_click_save(clause, index)}/>;
            }
            else {
                return <RuleClause clause={clause}
                                   index={index}
                                   key={`rules_field_clause_${index}`}
                                   on_click={index => this.on_click_select(index)}/>;
            }
        });

        return <div className='Field'>
            <div className='Label'>
                Rules
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#rules'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to include or exclude jobs in pipelines. Rules are evaluated in order until the first match.
                    When matched, the job is either included or excluded from the pipeline, depending on the
                    configuration.
                </div>
            </div>
            <div>
                <button onClick={() => this.on_click_add()}>Add clause</button>
                <br/>
                <br/>
                <div className='RuleClauses'>{clauses}</div>
            </div>
        </div>;
    }

    on_click_add() {
        const { rules } = this.state;
        rules.push({});
        this.setState({ rules, selected_index: rules.length - 1 });
    }

    on_click_delete(index) {
        if (window.confirm('Confirm to delete.')) {
            const { rules } = this.state;
            rules.splice(index, 1);
            this.setState({ rules, selected_index: null });
            pipeline_set_job_rules(model.job_editor_name, rules);
        }
    }

    on_click_save(clause, index) {
        const { rules } = this.state;
        rules[index] = clause;
        this.setState({ rules, selected_index: null });
        pipeline_set_job_rules(model.job_editor_name, rules);
    }

    on_click_select(index) {
        this.setState({ selected_index: index });
    }
}

export default RulesField;
