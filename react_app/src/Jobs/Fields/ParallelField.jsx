import React from 'react';
import model, { pipeline_set_job_parallel } from '../../model';
import StringListInput from '../../Inputs/StringListInput';

class ParallelField extends React.Component {
    constructor(props) {
        super(props);
        const original_value = model.pipeline[model.job_editor_name].parallel;
        let mode = 'None';
        let count = null;
        let matrix = null;
        if (typeof original_value === 'object') {
            mode = 'Matrix';
            matrix = original_value.matrix;
        }
        else if (Number.isInteger(original_value)) {
            mode = 'Count';
            count = original_value;
        }
        this.state = { mode, count, matrix };
    }

    render() {
        const { mode, count, matrix } = this.state;

        let additional_fields = null;

        if (mode === 'Count') {
            additional_fields = <div className='SubField'>
                <div className='Label'>Count</div>
                <input className='Input'
                       defaultValue={count}
                       min={2}
                       max={50}
                       onChange={event => this.on_change_simple(event)}
                       placeholder='Number between 2 and 50'
                       type='number'/>
            </div>;
        }
        else if (mode === 'Matrix') {
            additional_fields = <div className='MatrixList'>
                {matrix.map((matrix_item, matrix_index) => {
                    return <div className='MatrixItem'
                                key={`Matrix_Item_Index_${matrix_index}`}>
                        {
                            Object.keys(matrix_item).map(key => {
                                const values = matrix_item[key] || [];
                                return <div className='Variable SubForm'>
                                    <div className='SubField'>
                                        <div className='Label'>Name</div>
                                        <input className='Input'
                                               defaultValue={key}
                                               onChange={event => this.on_change_variable_name(matrix_index, key, event.target.value)}
                                               placeholder='Variable name'/>
                                    </div>
                                    <div className='SubField'>
                                        <div className='Label'>Values</div>
                                        <StringListInput always_edit_mode={true}
                                                         list={values}
                                                         on_change={list => this.on_change_variable_values(matrix_index, key, list)}/>
                                    </div>
                                    {Object.keys(matrix_item).length > 2 &&
                                    <button className='Danger'
                                            onClick={() => this.on_click_variable_remove(matrix_index, key)}>
                                        Remove variable
                                    </button>}
                                </div>;
                            })
                        }
                        <button onClick={() => this.on_click_variable_add(matrix_index)}>Add variable</button>
                        <button className='Danger'
                                onClick={() => this.on_click_matrix_remove(matrix_index)}>Remove matrix
                        </button>
                    </div>;
                })}
                <button onClick={() => this.on_click_matrix_add()}>Add matrix</button>
            </div>;
        }

        return <div className='Field'>
            <div className='Label'>
                Parallel
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#parallel'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Allows you to configure how many instances of a job to run in parallel. This value has to be greater
                    than or equal to two (2) and less than or equal to 50.
                </div>
            </div>
            <div className='SubForm'>
                <div className='SubField'>
                    <div className='Label'>Mode</div>
                    <select className='Input'
                            defaultValue={mode}
                            onChange={event => this.on_change_mode(event)}>
                        <option>None</option>
                        <option>Count</option>
                        <option>Matrix</option>
                    </select>
                </div>
                {additional_fields}
            </div>
        </div>;
    }

    on_change_mode(event) {
        const mode = event.target.value;
        if (mode === 'Count') {
            this.setState({ mode, count: model.pipeline[model.job_editor_name].parallel, matrix: null });
        }
        else if (mode === 'Matrix') {
            this.setState({ mode, count: null, matrix: [] });
        }
        else {
            this.setState({ mode, count: null, matrix: null });
        }
    }

    on_change_simple(event) {
        const value = (event.target.value && parseInt(event.target.value)) || null;
        pipeline_set_job_parallel(model.job_editor_name, value);
    }

    on_click_matrix_add() {
        const { matrix } = this.state;
        matrix.push({ X: [], Y: [] });
        this.setState({ matrix });
        pipeline_set_job_parallel(model.job_editor_name, { matrix });
    }

    on_click_matrix_remove(matrix_index) {
        if (window.confirm('Confirm to delete?')) {
            const { matrix } = this.state;
            matrix.splice(matrix_index, 1);
            this.setState({ matrix });
            pipeline_set_job_parallel(model.job_editor_name, { matrix });
        }
    }

    on_click_variable_add(matrix_index) {
        const { matrix } = this.state;
        const key = `VARIABLE_${Object.keys(matrix[matrix_index]).length + 1}`;
        matrix[matrix_index][key] = [];
        this.setState({ matrix });
        pipeline_set_job_parallel(model.job_editor_name, { matrix });
    }

    on_click_variable_remove(matrix_index, key) {
        if (window.confirm('Confirm to delete?')) {
            const { matrix } = this.state;
            delete matrix[matrix_index][key];
            this.setState({ matrix });
            pipeline_set_job_parallel(model.job_editor_name, { matrix });
        }
    }

    on_change_variable_name(matrix_index, old_key, new_key) {
        const { matrix } = this.state;
        const swap_source = matrix[matrix_index];
        const swap_target = {};
        Object.keys(swap_source).forEach(current_key => {
            if (current_key === old_key) swap_target[new_key] = swap_source[current_key];
            else swap_target[current_key] = swap_source[current_key];
        });
        matrix[matrix_index] = swap_target;
        this.setState({ matrix });
        pipeline_set_job_parallel(model.job_editor_name, { matrix });
    }

    on_change_variable_values(matrix_index, key, values) {
        const { matrix } = this.state;
        matrix[matrix_index][key] = values;
        this.setState({ matrix });
        pipeline_set_job_parallel(model.job_editor_name, { matrix });
    }
}

export default ParallelField;
