import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_artifacts } from '../../model';

class ArtifactsField extends React.Component {
    constructor(props) {
        super(props);
        const default_artifacts = {
            paths: [],
            exclude: [],
            untracked: false,
            when: 'always',
            name: '',
            expose_as: '',
        };
        this.state = { artifacts: model.pipeline[model.job_editor_name].artifacts || default_artifacts };
    }

    render() {
        const { artifacts } = this.state;
        return <div className='Field'>
            <div className='Label'>
                Artifacts
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts'>Docs</a>
                <div className='Hint'>
                    Used to specify a list of files and directories which should be attached to the job and are
                    available for download in the GitLab UI
                </div>
            </div>
            <div className='SubForm'>
                <div className='SubField'>
                    <div className='Label'>Paths</div>
                    <StringListInput key_prefix='Artifacts_Paths_Key'
                                     list={artifacts.paths}
                                     on_change={list => this.on_change_paths(list)}
                                     placeholder_add='Add path for file or folder, wildcards allowed'
                                     value_prefix='./'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Excludes</div>
                    <StringListInput key_prefix='Artifacts_Excludes_Key'
                                     list={artifacts.paths}
                                     on_change={list => this.on_change_exclude(list)}
                                     placeholder_add='Add path for file or folder, wildcards allowed'
                                     value_prefix='./'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Name</div>
                    <input className='Input'
                           onChange={event => this.on_change_name(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Expose As</div>
                    <input className='Input'
                           onChange={event => this.on_change_expose_as(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Untracked</div>
                    <select className='Input'
                            defaultValue={artifacts.untracked}
                            onChange={event => this.on_change_untracked(event)}>
                        <option>False</option>
                        <option>True</option>
                    </select>
                </div>
                <div className='SubField'>
                    <div className='Label'>When</div>
                    <select className='Input'
                            defaultValue={artifacts.when}
                            onChange={event => this.on_change_when(event)}>
                        <option value='on_success'>On success</option>
                        <option value='on_failure'>On failure</option>
                        <option value='always'>Always</option>
                    </select>
                </div>
            </div>
        </div>;
    }

    on_change_paths(list) {
        const { artifacts } = this.state;
        artifacts.paths = list;
        this.setState({ artifacts });
        this.save_changes();
    }

    on_change_exclude(list) {
        const { artifacts } = this.state;
        artifacts.exclude = list;
        this.setState({ artifacts });
        this.save_changes();
    }

    on_change_untracked(event) {
        const { artifacts } = this.state;
        artifacts.untracked = event.target.value === 'True';
        this.setState({ artifacts });
        this.save_changes();
    }

    on_change_when(event) {
        const { artifacts } = this.state;
        artifacts.when = event.target.value;
        this.setState({ artifacts });
        this.save_changes();
    }

    on_change_name(event) {
        const { artifacts } = this.state;
        artifacts.name = event.target.value;
        this.setState({ artifacts });
        this.save_changes();
    }

    on_change_expose_as(event) {
        const { artifacts } = this.state;
        artifacts.expose_as = event.target.value;
        this.setState({ artifacts });
        this.save_changes();
    }

    save_changes() {
        const { artifacts } = this.state;
        if (!artifacts.untracked) delete artifacts.untracked;
        if (!artifacts.when || artifacts.when === 'always') delete artifacts.when;
        if (!artifacts.name) delete artifacts.name;
        if (!artifacts.expose_as) delete artifacts.expose_as;
        pipeline_set_job_artifacts(model.job_editor_name, artifacts);
    }
}

export default ArtifactsField;
