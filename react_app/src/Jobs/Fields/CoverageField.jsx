import React from 'react';
import model, { pipeline_set_job_coverage } from '../../model';

class CoverageField extends React.Component {
    render() {
        const value = model.pipeline[model.job_editor_name].coverage || '';

        return <label className='Field'>
            <div className='Label'>
                Coverage
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#coverage'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Allows you to configure how code coverage will be extracted from the job output.
                    Regular expressions are the only valid kind of value expected here.
                </div>
            </div>
            <div>
                <input className='Input'
                       defaultValue={value}
                       onChange={event => this.on_change(event)}
                       placeholder="Regular expression, for example '/Code coverage: \d+\.\d+/'"
                       type='text'/>
            </div>
        </label>;
    }

    on_change(event) {
        const value = event.target.value;
        pipeline_set_job_coverage(model.job_editor_name, value);
    }
}

export default CoverageField;
