import React from 'react';
import model, { pipeline_set_job_retry } from '../../model';

class RetryField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            retry: model.pipeline[model.job_editor_name].retry || { max: 0, when: [] },
        };
    }

    render() {
        const { retry } = this.state;

        return <div className='Field'>
            <div className='Label'>
                Retry
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#retry'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Configure how many times a job is going to be retried in case of a failure
                </div>
            </div>
            <div className='SubForm'>
                <label className='SubField'>
                    <div className='Label'>Max</div>
                    <input className='Input'
                           defaultValue={retry.max}
                           onChange={event => this.on_change_max(event)}
                           type='number'/>
                </label>
                <label className='SubField'>
                    <div className='Label'>When</div>
                    <select className='Input'
                            defaultValue={retry.when}
                            disabled={!retry.max}
                            multiple
                            onChange={event => this.on_change_when(event)}
                            size={14}>
                        <option value='always'>Always</option>
                        <option value='unknown_failure'>Unknown failure</option>
                        <option value='script_failure'>Script failure</option>
                        <option value='api_failure'>API failure</option>
                        <option value='stuck_or_timeout_failure'>Stuck or timeout failure</option>
                        <option value='runner_system_failure'>Runner system failure</option>
                        <option value='missing_dependency_failure'>Missing dependency failure</option>
                        <option value='runner_unsupported'>Runner unsupported</option>
                        <option value='stale_schedule'>Stale schedule</option>
                        <option value='job_execution_timeout'>Job execution timeout</option>
                        <option value='archived_failure'>Archived failure</option>
                        <option value='unmet_prerequisites'>Unmet prerequisites</option>
                        <option value='scheduler_failure'>Scheduler failure</option>
                        <option value='data_integrity_failure'>Data integrity failure</option>
                    </select>
                </label>
            </div>
        </div>;
    }

    on_change_max(event) {
        const value = event.target.value;
        const { retry } = this.state;
        retry.max = value;
        pipeline_set_job_retry(model.job_editor_name, retry);
        this.setState({ retry });
    }

    on_change_when(event) {
        const selected_values = [...event.target.selectedOptions].map(o => o.value);
        const { retry } = this.state;
        retry.when = selected_values;
        pipeline_set_job_retry(model.job_editor_name, retry);
        this.setState({ retry });
    }
}

export default RetryField;
