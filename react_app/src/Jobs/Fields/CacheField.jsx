import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_cache } from '../../model';

class CacheField extends React.Component {
    constructor(props) {
        super(props);
        const default_cache = {
            key: '',
            paths: [],
            policy: false,
            untracked: false,
        };
        this.state = { cache: model.pipeline[model.job_editor_name].cache || default_cache };
    }

    render() {
        const { cache } = this.state;
        return <div className='Field'>
            <div className='Label'>
                Cache
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#cache'>Docs</a>
                <div className='Hint'>
                    Used to specify a list of files and directories which should be cached between jobs
                </div>
            </div>
            <div className='SubForm'>
                <div className='SubField'>
                    <div className='Label'>Key</div>
                    <input className='Input'
                           onChange={event => this.on_change_key(event)}/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Paths</div>
                    <StringListInput key_prefix='Cache_Paths_Key'
                                     list={cache.paths}
                                     on_change={list => this.on_change_paths(list)}
                                     placeholder_add='Add path for file or folder, wildcards allowed'
                                     value_prefix='./'/>
                </div>
                <div className='SubField'>
                    <div className='Label'>Policy</div>
                    <select className='Input'
                            defaultValue={cache.policy || 'pull-push'}
                            onChange={event => this.on_change_policy(event)}>
                        <option value='pull'>Pull</option>
                        <option value='push'>Push</option>
                        <option value='pull-push'>Pull-Push</option>
                    </select>
                </div>
                <div className='SubField'>
                    <div className='Label'>Untracked</div>
                    <select className='Input'
                            defaultValue={cache.untracked}
                            onChange={event => this.on_change_untracked(event)}>
                        <option>False</option>
                        <option>True</option>
                    </select>
                </div>
            </div>
        </div>;
    }

    on_change_paths(list) {
        const { cache } = this.state;
        cache.paths = list;
        this.setState({ cache });
        this.save_changes();
    }

    on_change_key(event) {
        const { cache } = this.state;
        cache.key = event.target.value;
        this.setState({ cache });
        this.save_changes();
    }

    on_change_policy(event) {
        const { cache } = this.state;
        cache.policy = event.target.value;
        this.setState({ cache });
        this.save_changes();
    }

    on_change_untracked(event) {
        const { cache } = this.state;
        cache.untracked = event.target.value === 'True';
        this.setState({ cache });
        this.save_changes();
    }

    save_changes() {
        const { cache } = this.state;
        if (!cache.policy || cache.policy === 'pull-push') delete cache.policy;
        if (!cache.untracked) delete cache.untracked;
        pipeline_set_job_cache(model.job_editor_name, cache);
    }
}

export default CacheField;
