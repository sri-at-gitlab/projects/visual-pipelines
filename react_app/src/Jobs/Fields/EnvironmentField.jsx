import React from 'react';
import model, {
    get_all_job_names,
    pipeline_set_job_environment_action,
    pipeline_set_job_environment_auto_stop_in,
    pipeline_set_job_environment_kubernetes_namespace,
    pipeline_set_job_environment_name,
    pipeline_set_job_environment_on_stop,
    pipeline_set_job_environment_url,
} from '../../model';

class EnvironmentField extends React.Component {
    render() {
        const value = model.pipeline[model.job_editor_name].environment;
        const job_names = get_all_job_names();
        return <div className='Field'>
            <div className='Label'>
                Environment
                <a href='https://docs.gitlab.com/ee/ci/yaml/#environment' target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to define that a job deploys to a specific environment. If environment is specified and no
                    environment under that name exists, a new one will be created automatically.
                </div>
            </div>
            <div className='SubForm'>
                <div className='SubField'>
                    <div className='Label'>Name</div>
                    <input className='Input'
                           defaultValue={(value && value.name) || ''}
                           onChange={event => this.on_change_name(event)}
                           type='text'/>
                </div>
                <label className='SubField'>
                    <div className='Label'>Url</div>
                    <input className='Input'
                           disabled={!value}
                           onChange={event => this.on_change_url(event)}
                           type='text'/>
                </label>
                <label className='SubField'>
                    <div className='Label'>On Stop</div>
                    <select className='Input'
                            defaultValue={null}
                            disabled={!value}
                            onChange={event => this.on_change_on_stop(event)}>
                        <option value={null}>None</option>
                        {job_names.map(name => <option key={`on_stop_option_${name}`}
                                                       value={name}>{name}</option>)}
                    </select>
                </label>
                <label className='SubField'>
                    <div className='Label'>Action</div>
                    <select className='Input'
                            defaultValue={null}
                            disabled={!value}
                            onChange={event => this.on_change_action(event)}>
                        <option value={null}>None</option>
                        <option value='start'>Start</option>
                        <option value='prepare'>Prepare</option>
                        <option value='stop'>Stop</option>
                    </select>
                </label>
                <label className='SubField'>
                    <div className='Label'>Auto Stop In</div>
                    <input className='Input'
                           disabled={!value}
                           onChange={event => this.on_change_auto_stop_in(event)}/>
                </label>
                <label className='SubField'>
                    <div className='Label'>Kubernetes Namespace</div>
                    <input className='Input'
                           disabled={!value}
                           onChange={event => this.on_change_kubernetes_namespace(event)}/>
                </label>
            </div>
        </div>;
    }

    on_change_name(event) {
        const value = event.target.value;
        pipeline_set_job_environment_name(model.job_editor_name, value);
    }

    on_change_url(event) {
        const value = event.target.value;
        pipeline_set_job_environment_url(model.job_editor_name, value);
    }

    on_change_on_stop(event) {
        const value = event.target.value;
        pipeline_set_job_environment_on_stop(model.job_editor_name, value);
    }

    on_change_action(event) {
        const value = event.target.value;
        pipeline_set_job_environment_action(model.job_editor_name, value);
    }

    on_change_auto_stop_in(event) {
        const value = event.target.value;
        pipeline_set_job_environment_auto_stop_in(model.job_editor_name, value);
    }

    on_change_kubernetes_namespace(event) {
        const value = event.target.value;
        pipeline_set_job_environment_kubernetes_namespace(model.job_editor_name, value);
    }
}

export default EnvironmentField;
