import React from 'react';

import BaseTabs from './BaseTabs';

import './SideTabs.css';

const SideTabs = ({ tabs }) => <BaseTabs tabs={tabs} tab_location='side'/>;

export default SideTabs;
