import classnames from 'classnames';
import React from 'react';

class BaseTabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = { selected_index: 0 };
    }

    render() {
        const { tabs, tab_location } = this.props;
        let { selected_index } = this.state;

        if (!tabs[selected_index]) selected_index = 0;

        const selected = tabs[selected_index].content;

        const tab_buttons = tabs.map((tab, index) => {
            return <button className={classnames({ Active: selected_index === index })}
                           key={`SideTabs_TabButtons_Button_${tab}_${index}`}
                           onClick={() => this.setState({ selected_index: index })}>
                {tab.title}
            </button>;
        });

        const classes = classnames({ SideTabs: tab_location === 'side', TopTabs: tab_location === 'top' });

        return <div className={classes}>
            <div className='TabButtons'>{tab_buttons}</div>
            <div className='SelectedTab'>{selected}</div>
        </div>;
    }
}

export default BaseTabs;
