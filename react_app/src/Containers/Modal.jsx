import React from 'react';

import model, { job_editor_set_name, pipelib_set_minimized, text_editor_set_minimized } from '../model';

const on_click = () => {
    text_editor_set_minimized(true);
    pipelib_set_minimized(true);
    job_editor_set_name(null);
};

const Modal = () => {
    const is_text_editor_hidden = model.text_editor_minimized;
    const is_pipelib_hidden = model.pipelib_minimized;
    const is_job_editor_hidden = !model.job_editor_name;
    const hide_model = is_text_editor_hidden && is_pipelib_hidden && is_job_editor_hidden;
    if (hide_model) return null;
    return <div id='Modal' onClick={on_click}/>;
};

export default Modal;
