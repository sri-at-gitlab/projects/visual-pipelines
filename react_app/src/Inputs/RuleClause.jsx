import React from 'react';

class RuleClause extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clause: props.clause,
            index: props.index,
        };
    }

    render() {
        const { index } = this.state;
        const { clause, on_click } = this.props;

        let row_if = null;
        if (clause.if) {
            row_if = <div className='Row'>
                <dt>If:</dt>
                <dd>{clause.if}</dd>
            </div>;
        }

        let row_changes = null;
        if (clause.changes) {
            row_changes = <div className='Row'>
                <dt>Changes:</dt>
                <dd>{clause.changes.map(item => <div key={`rules_clause_changes_${item}`}>{item}</div>)}</dd>
            </div>;
        }

        let row_exists = null;
        if (clause.exists) {
            row_exists = <div className='Row'>
                <dt>Exists:</dt>
                <dd>{clause.exists.map(item => <div key={`rules_clause_exists_${item}`}>{item}</div>)}</dd>
            </div>;
        }

        let row_when = null;
        if (clause.when) {
            row_when = <div className='Row'>
                <dt>When:</dt>
                <dd>{clause.when}</dd>
            </div>;
        }

        let row_start_in = null;
        if (clause.start_in) {
            row_start_in = <div className='Row'>
                <dt>Start in:</dt>
                <dd>{clause.start_in}</dd>
            </div>;
        }

        const row_allow_failure = <div className='Row'>
            <dt>Allow failure:</dt>
            <dd>{clause.allow_failure ? 'true' : 'false'}</dd>
        </div>;

        return <dl className='RuleClause'
                   onClick={() => on_click(index)}>
            {row_if}
            {row_changes}
            {row_exists}
            {row_when}
            {row_start_in}
            {row_allow_failure}
        </dl>;
    }
}

export default RuleClause;
