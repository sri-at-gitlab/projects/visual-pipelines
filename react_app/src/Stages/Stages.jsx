import React from 'react';
import {
    get_drag_type,
    get_jobs_for_stage,
    get_stages,
    pipeline_create_job,
    pipeline_create_stage,
    pipeline_remove_stage,
    pipeline_reorder_stage,
    pipeline_set_job_stage,
    set_drag_type,
} from '../model';
import { JobItem } from '../Jobs/Jobs';
import StageNameEditor from './StageNameEditor';
import './Stages.css';

export const drag_data_separator = '________';

const generate_on_drag_over = stage_index => {
    return event => {
        event.preventDefault();
        const drag_type = get_drag_type();
        if (drag_type === 'stage') {
            const drop_zones = document.querySelectorAll(`.StageDropZone`);
            drop_zones.forEach(drop_zone => drop_zone.classList.remove('Active'));
            const drop_zone = document.querySelector(`.StageDropZone${stage_index}`);
            drop_zone.classList.add('Active');
        }
        else if (drag_type === 'job') {
            const is_drop_left = event.target.classList.contains('DropLeft');
            const is_drop_right = event.target.classList.contains('DropRight');
            if (is_drop_left || is_drop_right) {
                event.target.parentNode.classList.add('Active');
            }
        }
    };
};

const generate_on_drag_leave_and_on_drop_capture = stage_index => {
    return event => {
        event.preventDefault();
        const drag_type = get_drag_type();
        if (drag_type === 'stage') {
            const drop_zones = document.querySelectorAll(`.StageDropZone`);
            drop_zones.forEach(drop_zone => drop_zone.classList.remove('Active'));
        }
        else if (drag_type === 'job') {
            const is_drop_left = event.target.classList.contains('DropLeft');
            const is_drop_right = event.target.classList.contains('DropRight');
            if (is_drop_left || is_drop_right) {
                event.target.parentNode.classList.remove('Active');
            }
        }
    };
};

const generate_on_stage_drop = stage_index => {
    return event => {
        event.preventDefault();
        const drop_data = event.dataTransfer.getData('text/plain');
        const stage_dropped = drop_data.indexOf('stage_') === 0;
        if (stage_dropped) {
            const current_index = parseInt(drop_data.split(drag_data_separator)[1]);
            pipeline_reorder_stage(current_index, stage_index);
            const drop_zone_left_right = document.querySelectorAll('.DropLeft, .DropRight');
            drop_zone_left_right.forEach(drop_zone => drop_zone.style.zIndex = 1);
        }
    };
};

const Stage = ({ index, stage }) => {
    const on_drag_start = event => {
        event.target.classList.add('Active');
        event.dataTransfer.setData('text/plain', `stage${drag_data_separator}${index}`);
        event.dataTransfer.dropEffect = 'move';
        set_drag_type('stage');
        const drop_zone_left_right = document.querySelectorAll('.DropLeft, .DropRight');
        drop_zone_left_right.forEach(drop_zone => drop_zone.style.zIndex = 3);
    };

    const on_drag_end = event => event.target.classList.remove('Active');

    const on_job_drop = event => {
        event.preventDefault();
        const drop_data = event.dataTransfer.getData('text/plain');
        const job_dropped = drop_data.indexOf('job_') === 0;
        if (job_dropped) {
            const job_name = drop_data.split(drag_data_separator)[1];
            pipeline_set_job_stage(job_name, index);
            const drop_zone_left_right = document.querySelectorAll('.DropLeft, .DropRight');
            drop_zone_left_right.forEach(drop_zone => drop_zone.style.zIndex = 1);
        }
    };

    const on_drag_over_left = generate_on_drag_over(index);
    const on_drag_leave_left = generate_on_drag_leave_and_on_drop_capture(index);
    const on_stage_drop_left = generate_on_stage_drop(index);
    const on_drop_capture_left = generate_on_drag_leave_and_on_drop_capture(index);

    const on_drag_over_right = generate_on_drag_over(index + 1);
    const on_drag_leave_right = generate_on_drag_leave_and_on_drop_capture(index + 1);
    const on_stage_drop_right = generate_on_stage_drop(index + 1);
    const on_drop_capture_right = generate_on_drag_leave_and_on_drop_capture(index + 1);

    const jobs = get_jobs_for_stage(stage);
    const job_items = jobs.map(job => <JobItem config={job.config}
                                               key={`job_item_${job.name}`}
                                               name={job.name}/>);

    let remove_button = null;
    if (jobs.length === 0) {
        remove_button = <button className='Danger'
                                onClick={() => pipeline_remove_stage(index)}>
            Remove stage
        </button>;
    }

    return <>
        <div className={`StageDropZone StageDropZone${index}`}
             data-index={index}
             onDragOver={on_drag_over_left}
             onDragLeave={on_drag_leave_left}
             onDrop={on_stage_drop_left}
             onDropCapture={on_drop_capture_left}/>
        <div className='Stage'
             draggable={true}
             id={`stage-${index}`}
             onDragStart={on_drag_start}
             onDragEnd={on_drag_end}
             onDrop={on_job_drop}
             onMouseDown={event => event.target.parentNode.classList.add('Active')}
             onMouseUp={event => event.target.parentNode.classList.remove('Active')}>
            <div className='DropLeft'
                 onDragOver={on_drag_over_left}
                 onDragLeave={on_drag_leave_left}
                 onDrop={on_stage_drop_left}
                 onDropCapture={on_drop_capture_left}/>
            <div className='DropRight'
                 onDragOver={on_drag_over_right}
                 onDragLeave={on_drag_leave_right}
                 onDrop={on_stage_drop_right}
                 onDropCapture={on_drop_capture_right}/>
            <StageNameEditor index={index}/>
            <div className='JobList'>{job_items}</div>
            <button className='Primary'
                    onClick={() => pipeline_create_job(stage)}>
                Add job
            </button>
            {remove_button}
        </div>
    </>;
};

const stage_mapper = (stage, index) => <Stage index={index}
                                              key={`stage-${index}`}
                                              stage={stage}/>;

const Stages = () => {
    const stages = get_stages();
    if (!Array.isArray(stages)) return null;
    const stage_list = stages.map(stage_mapper);
    const last_drop_zone_styles = stages.length !== 0 ? { background: '#fff' } : undefined;
    return <div id='Stages'>
        <div className='StageList'>{stage_list}</div>
        <div className={`StageDropZone StageDropZone${stages.length}`}
             data-index={stages.length}
             onDragOver={generate_on_drag_over(stages.length)}
             onDragLeave={generate_on_drag_leave_and_on_drop_capture(stages.length)}
             onDrop={generate_on_stage_drop(stages.length)}
             onDropCapture={generate_on_drag_leave_and_on_drop_capture(stages.length)}
             style={last_drop_zone_styles}/>
        <button onClick={() => pipeline_create_stage()}>Add stage</button>
    </div>;
};

export default Stages;
