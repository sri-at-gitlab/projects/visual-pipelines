import React from 'react';
import model, { pipeline_update_stage } from '../model';
import './StageNameEditor.css';

export default class StageNameEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edit_mode: false,
            field_value: model.pipeline.stages[props.index],
        };
    }

    render() {
        const { index } = this.props;
        const { edit_mode } = this.state;

        if (edit_mode) {
            return <div className='StageNameEditor'>
                <input autoFocus
                       type='text'
                       defaultValue={model.pipeline.stages[index]}
                       onBlur={() => this.field_on_blur()}
                       onChange={event => this.setState({ field_value: event.target.value })}
                       onFocus={event => {
                           const target = event.target;
                           this.setState({ field_value: model.pipeline.stages[index] }, () => target.select());
                       }}
                       onKeyDown={event => this.field_on_key_down(event)}/>
            </div>;
        }
        else {
            return <div className='StageNameEditor'
                        onClick={() => this.setState({ edit_mode: true })}>
                <span className='Name'>{model.pipeline.stages[index]}</span>
                &nbsp;
                <span className='Index'>#{index + 1}</span>
            </div>;
        }
    }

    field_on_blur() {
        const { index } = this.props;
        const original_value = model.pipeline.stages[index];
        const { field_value } = this.state;
        if (field_value !== original_value) {
            pipeline_update_stage(index, field_value);
        }
        this.setState({ edit_mode: false });
    }

    field_on_key_down(event) {
        const original_value = model.pipeline.stages[this.props.index];

        if (event.key === 'Escape') {
            this.setState({ field_value: original_value }, () => {
                document.querySelector('.StageNameEditor input').blur();
            });
        }
        else if (event.key === 'Enter') {
            document.querySelector('.StageNameEditor input').blur();
        }
    }
}
